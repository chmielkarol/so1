#include <ncurses.h>
#include <pthread.h>
#include <unistd.h>
#include <cstdlib>

int X, Y;

struct element{
	int x = -1;
	int y = -1;
	element* next = NULL;
};

element* circles = NULL;
element** last = &circles;

bool end = false;

float v_factors[7][2] = {{-0.92, 0.38}, {-0.71, 0.71}, {-0.38, 0.92}, {1, 1}, {0.38, 0.92}, {0.71, 0.71}, {0.92, 0.38}}; //v[x][y]

void* circle(void* tab){
	element* ptr = new element;
	float x = X/2;
	float y = 0;
	ptr->x = x;
	ptr->y = y;
	*last = ptr;
	last = &(ptr->next);
	float v_x = *((float*) tab);
	float v_y = *((float*) tab + 1);
	//until v^2>v_min^2
	while(((v_x*v_x + v_y*v_y) > 0.2) && (!end)){
		//print circle
			ptr->x = x;
			ptr->y = y;
		//move
		x += v_x;
		y += v_y;
		//bounce
		if((x < 0) || (x > X)){
			v_x = -v_x;
			x += v_x;
			//change velocity
			v_x*=0.5;
			v_y*=0.5;
		}
		if((y < 0) || (y > Y)){
			v_y = -v_y;
			y += v_y;
			//change velocity
			v_x*=0.5;
			v_y*=0.5;
		}
		//sleep
		usleep(40000);
	}
	ptr->x = -1;
	ptr->y = -1;
	//circles = ptr->next;
}

void* draw(void* x){
	initscr();
	getmaxyx(stdscr, Y, X);
	curs_set(0);
	timeout(1);
	while(!end){
		if(getch()=='x') end = true;
		clear();

		element* ptr = circles;
		while(ptr!=NULL){
			mvprintw(ptr->y, ptr->x, "o");
			ptr = ptr->next;
		}

		refresh();
		usleep(40000);
	}
	endwin();
	element* ptr = circles;
	while(ptr!=NULL){
		element* temp = ptr;
		ptr = ptr->next;
		delete ptr;
	}
}

void* create_circles(void* ptr){
	usleep(100000);
	while(!end){
		pthread_t thread;
		float tab[2];

		int direction = rand()%7;
		tab[0] = 2 * v_factors[direction][0];
		tab[1] = 2 * v_factors[direction][1];
		
		pthread_create(&thread, NULL, circle, (void*)tab);
		sleep(1);
	}
}

int main(){

	pthread_t thread, thread2;
	pthread_create(&thread2, NULL, draw, (void*)new int);
	pthread_create(&thread, NULL, create_circles, (void*)new int);

	pthread_join(thread2, NULL);
	pthread_join(thread, NULL);

	return 0;
}
